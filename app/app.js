import Vue from "nativescript-vue";

import App from "./components/App.vue";

new Vue({
    render: h => h(App)
}).$start();

// new Vue({

//     template: `
//         <Frame>
//             <HelloWorld />
//         </Frame>`,

//     components: {
//         HelloWorld
//     }
// }).$start();